CPATH = testin kansiopolku
CONF = testitiedosto

$PWD on Linuxin ympäristömuuttuja, joka viittaa. Voit vaihtaa tilalle absoluuttisen tai relatiivisen polun.

docker run -it --rm --name sc -e CPATH=$PWD -e CONF=testi.conf -v $PWD:$PWD registry.gitlab.com/jamkit/fMBT-container/fmbt