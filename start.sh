#!/bin/bash

set -e

CPATH=${CPATH:=}
CONF=${CONF:=}

Xvfb -ac :99 -screen 0 1920x1200x16 &
export DISPLAY=:99

cd $CPATH

fmbt -l test.log $CONF
fmbt-log test.log
