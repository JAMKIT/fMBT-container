# fMBT installation dockerfile
# Mikko Siloaho 16.2.2017

FROM ubuntu:latest

RUN apt-get -y update && apt-get install -y -qq wget &&\
    apt-get -y install software-properties-common &&\
    apt-add-repository -y ppa:antti-kervinen/fmbt-devel &&\
    apt-get -y update &&\
    apt-get -y install fmbt* &&\
    apt-get -y update &&\
    apt-get -y install python-pip &&\
    apt-get install -y firefox &&\
    apt-get -y install xvfb &&\
    yes | pip install selenium

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.14.0/geckodriver-v0.14.0-linux64.tar.gz &&\
    tar xfvz geckodriver-v0.14.0-linux64.tar.gz && rm -f geckodriver-v0.14.0-linux64.tar.gz &&\
    chmod +777 geckodriver && mv -f geckodriver /usr/bin/geckodriver

COPY testi.sh /usr/local/bin/start.sh

RUN chmod -R 777 /usr/local/bin/start.sh

ENTRYPOINT ["/usr/local/bin/start.sh"]
